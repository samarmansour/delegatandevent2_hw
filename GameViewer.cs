﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game_DelegatEvent
{
    public class GameViewer
    {
        public void GoodSpaceShipHPChangedEventHandler(object sender, PointsEventArgs points)
        {
            if (sender is SpaceQuestGameManager)
            {
                Console.WriteLine($"sender is: {sender}");
                Console.WriteLine($"spaceship new location: {points.HitPoints}");
            }
        }

        public void GoodSpaceShipLocationChangedEventHandler(object sender, LocationEventArgs location)
        {
            if (sender is SpaceQuestGameManager)
            {
                Console.WriteLine($"sender is: {sender}");
                Console.WriteLine($"spaceship location: ({location.X}, {location.Y})");
            }
            
        }

        public void GoodSpaceShipDestroyedEventHandler(object sender, LocationEventArgs location)
        {
            if (sender is SpaceQuestGameManager)
            {
                Console.WriteLine($"sender is: {sender}");
                Console.WriteLine($"spaceship new location: {location.X},{location.Y}");
            }
        }

        public void BadShipExplodedEventHandler(object sender, BadShipExplodedEventArgs badShipExploded)
        {
            if (sender is SpaceQuestGameManager)
            {
                Console.WriteLine($"sender is: {sender}");
                Console.WriteLine($"Enemy ship exploded: {badShipExploded.NumberOfExplodedBadShips}");
            }
        }

        public void LevelUpReachedEventHandler(object sender, LevelEventArgs level)
        {
            if (sender is SpaceQuestGameManager)
            {
                Console.WriteLine($"sender is: {sender}");
                Console.WriteLine($"spaceship new location: {level.CurrentLevel}");
            }
        }
    }
}

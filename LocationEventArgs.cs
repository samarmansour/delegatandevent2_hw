﻿namespace Game_DelegatEvent
{
    public class LocationEventArgs
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
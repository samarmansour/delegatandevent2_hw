﻿namespace Game_DelegatEvent
{
    public class LevelEventArgs
    {
        public int CurrentLevel { get; set; }
    }
}
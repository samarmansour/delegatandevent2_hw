﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game_DelegatEvent
{
    public class SpaceQuestGameManager
    {
        private int _goodSpaceShipHitPoints;
        private int _shipXLocation;
        private int _shipYLocation;
        private int _numberOfBadShips;
        private int _currentLevel;

        public event EventHandler<PointsEventArgs> GoodSpaceShipHPChanged;
        public event EventHandler<LocationEventArgs> GoodSpaceShipLocationChanged;
        public event EventHandler<LocationEventArgs> GoodSpaceShipDestroyed;
        public event EventHandler<BadShipExplodedEventArgs> BadShipExploded;
        public event EventHandler<LevelEventArgs> LevelUpReached;

        public SpaceQuestGameManager(int goodSpaceShipHitPoints, int shipXLocation, int shipYLocation, int numberOfBadShips)
        {
            _goodSpaceShipHitPoints = goodSpaceShipHitPoints;
            _shipXLocation = shipXLocation;
            _shipYLocation = shipYLocation;
            _numberOfBadShips = numberOfBadShips;
        }

        public void MoveSpaceShip(int newX, int newY)
        {
            Console.WriteLine($"Move spaceship: ({_shipXLocation}, {_shipYLocation})");
            Thread.Sleep(1000);

            if (GoodSpaceShipLocationChanged != null)
            {
                GoodSpaceShipLocationChanged("move ship Location",
                    new LocationEventArgs { X = newX });
                GoodSpaceShipLocationChanged("move ship Location",
                    new LocationEventArgs { Y = newY });
            }
        }

        public void GoodSpaceShipGotDamaged(int damage)
        {
            Console.WriteLine($"Move spaceship: ({_shipXLocation}, {_shipYLocation})");
            Thread.Sleep(1000);

            if (GoodSpaceShipLocationChanged != null)
            {
                GoodSpaceShipDestroyed("damage good ship",
                    new LocationEventArgs { X = damage });
                GoodSpaceShipDestroyed("damage good ship",
                    new LocationEventArgs { Y = damage });
            }
        }

        public void GoodSpaceShipGotExtreHP(int extra)
        {
            Console.WriteLine($"Good space ship got extra HP : {_goodSpaceShipHitPoints}");
            Thread.Sleep(1000);

            if (GoodSpaceShipHPChanged != null)
            {
                GoodSpaceShipHPChanged("damage good ship",
                    new PointsEventArgs {HitPoints = extra });
                LevelUpReached("Current level",
                    new LevelEventArgs { CurrentLevel = _currentLevel});
            }
        }

        public void EnemyShipDestroyed(int numberOfBadShipsDestroyed)
        {
            Console.WriteLine($"Enemy ship distroyed : {_numberOfBadShips}");
            Thread.Sleep(1000);

            if (BadShipExploded != null)
            {
                BadShipExploded("damage good ship",
                    new BadShipExplodedEventArgs { NumberOfExplodedBadShips = numberOfBadShipsDestroyed });
            }
        }

        private void OnGoodSpaceShipHpChanged()
        {
            if (GoodSpaceShipHPChanged != null)
            {
                GoodSpaceShipHPChanged.Invoke(this, new PointsEventArgs { HitPoints = _goodSpaceShipHitPoints});
            }
        }

        private void OnGoodSpaceShipLocationChanged()
        {
            if (GoodSpaceShipLocationChanged != null)
            {
                GoodSpaceShipLocationChanged.Invoke(this, new LocationEventArgs { X = _shipXLocation, Y = _shipYLocation });
            }
        }

        private void OnGoodSpaceShipDestroyed()
        {
            if (GoodSpaceShipDestroyed != null)
            {
                GoodSpaceShipDestroyed.Invoke(this, new LocationEventArgs { X = _shipXLocation, Y = _shipYLocation });

            }
        }

        private void OnBadShipExploded()
        {
            if (BadShipExploded != null)
            {
                BadShipExploded.Invoke(this, new BadShipExplodedEventArgs { NumberOfExplodedBadShips = _numberOfBadShips });
            }
        }

        private void OnLevelUpReached()
        {
            if (LevelUpReached != null)
            {
                LevelUpReached.Invoke(this, new LevelEventArgs { CurrentLevel = _currentLevel});
            }
        }

    }
}

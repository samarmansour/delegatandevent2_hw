﻿namespace Game_DelegatEvent
{
    public class BadShipExplodedEventArgs
    {
        public int NumberOfExplodedBadShips { get; set; }
    }
}
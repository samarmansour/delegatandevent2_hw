﻿namespace Game_DelegatEvent
{
    public class PointsEventArgs
    {
        public int HitPoints { get; set; }
    }
}